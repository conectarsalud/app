package com.tdp2.utils.extensions

@Suppress("unused")
fun String?.append(text: String?, separator: String = ", "): String {

    if (this.isNullOrBlank()) {
        return text ?: ""
    }

    if (text.isNullOrBlank()) {
        return this
    }

    return "$this$separator$text"
}

@Suppress("unused")
fun String?.appendAll(vararg list: String?, separator: String = ", "): String {

    val strings = mutableListOf<String>()

    if (this != null) {
        strings.add(this)
    }

    for (str in list) {
        if (!str.isNullOrBlank()) {
            strings.add(str)
        }
    }

    return strings.joinToString(separator = separator)
}

@Suppress("unused")
fun String.containsURL(): Boolean {

    val regex =
        Regex("((http://|https://)?(www.)?(([a-zA-Z0-9-]){2,}\\.){1,4}([a-zA-Z]){2,6}(/([a-zA-Z-_/.0-9#:?=&;,]*)?)?)")
    return regex.containsMatchIn(this)
}
