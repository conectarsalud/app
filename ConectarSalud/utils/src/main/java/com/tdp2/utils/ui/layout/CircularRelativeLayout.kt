package com.tdp2.utils.ui.layout

import android.content.Context
import android.graphics.Canvas
import android.graphics.Path
import android.util.AttributeSet
import android.widget.RelativeLayout

class CircularRelativeLayout : RelativeLayout {

    constructor(context: Context) : super(context) {
        setWillNotDraw(false)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setWillNotDraw(false)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        setWillNotDraw(false)
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            val halfWidth = (it.width / 2).toFloat()
            val halfHeight = (it.height / 2).toFloat()
            val radius = kotlin.math.max(halfWidth, halfHeight)
            val path = Path()
            path.addCircle(halfWidth, halfHeight, radius, Path.Direction.CCW)
            canvas.clipPath(path)
        }

        super.onDraw(canvas)
    }
}