package com.tdp2.utils.ui.openGraphView.tasks

import com.tdp2.utils.ui.openGraphView.model.OGData
import java.io.IOException
import java.util.concurrent.Callable
import java.util.concurrent.ExecutionException

class OpenGraphDataTask(callable: Callable<OGData>, listener: OnLoadListener<OGData>) :
    BaseTask<OGData>(callable, listener) {

    override fun done() {
        super.done()

        if (isCancelled) {
            return
        }
        val data = try {
            get()
        } catch (e: InterruptedException) {
            onError(e)
            return
        } catch (e: ExecutionException) {
            onError(e)
            return
        }
        if (data == null) {
            onError(IOException("No cache data."))
            return
        }
        onSuccess(data)
    }
}