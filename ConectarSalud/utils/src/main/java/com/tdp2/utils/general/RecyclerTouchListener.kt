package com.tdp2.utils.general

import android.content.Context
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import androidx.recyclerview.widget.RecyclerView

interface ClickListener {
    fun onClick(view: View, position: Int)
    fun onLongClick(view: View, position: Int, xx: Float, yy: Float)
}

@Suppress("unused")
class RecyclerTouchListener(val context: Context, val recycleView: RecyclerView, val clickListener: ClickListener) :
    RecyclerView.OnItemTouchListener {

    private val gestureDetector: GestureDetector

    init {
        gestureDetector = GestureDetector(context, object : GestureDetector.SimpleOnGestureListener() {
            override fun onSingleTapUp(e: MotionEvent): Boolean {
                return true
            }

            override fun onLongPress(e: MotionEvent) {

                val child = recycleView.findChildViewUnder(e.x, e.y)
                if (child != null) {
                    clickListener.onLongClick(child, recycleView.getChildAdapterPosition(child), e.x, e.y)
                }
            }
        })
    }

    override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
        rv.findChildViewUnder(e.x, e.y)?.apply {
            val position = rv.getChildAdapterPosition(this)
            if (position >= 0 && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(this, position)
            }
        }
        return false
    }

    override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {
    }

    override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
    }
}