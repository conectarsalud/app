package com.tdp2.utils.ui

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.text.InputFilter
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.tdp2.utils.R
import com.tdp2.utils.extensions.afterTextChanged
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

@Suppress("unused")
class CEditText : LinearLayout {

    private var mRootContainer: View
    private var mTouchView: View
    private var mTextInputLayout: TextInputLayout
    private var mTextInputEditText: TextInputEditText
    private var mErrorTextView: TextView
    private var mSuggestTextView: TextView
    private var mBorderView: LinearLayout
    private var mOnFocusChangeListener: OnFocusChangeListener? = null
    private val mColorEditTextIconFocused: ColorStateList
    private val mColorEditTextIconDefault: ColorStateList
    private var mEndDrawable: Drawable? = null
    private var mEndTintColor: ColorStateList? = null
    private var secondHint: String? = null
    private val secondHintMax
        get() = secondHint?.length ?: 0

    var text: String
        set(value) {
            mTextInputEditText.setText(value)
        }
        get() {
            return mTextInputEditText.text.toString()
        }

    var error: String?
        set(value) {
            mErrorTextView.visibility = when (value) {
                null -> View.GONE
                else -> View.VISIBLE
            }

            mErrorTextView.text = value
            updateBorderColor()
        }
        get() {
            return mErrorTextView.text.toString()
        }

    private var suggestionList: List<String> = listOf()
    private var delimiter: String = ""

    private val isSuggestEnabled: Boolean
        get() {
            return suggestionList.isNotEmpty()
        }

    var afterTextChanged: ((String) -> Unit)? = null
    var afterFocusChanged: ((Boolean) -> Unit)? = null

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {

        mRootContainer = inflate(context, R.layout.cedittext_view, this)
        mTouchView = mRootContainer.findViewById(R.id.touchView)
        mTextInputLayout = mRootContainer.findViewById(R.id.textLayout)
        mTextInputEditText = mRootContainer.findViewById(R.id.editText)
        mSuggestTextView = mRootContainer.findViewById(R.id.suggestTextView)
        mErrorTextView = mRootContainer.findViewById(R.id.errorTextView)
        mBorderView = mRootContainer.findViewById(R.id.borderView)

        mColorEditTextIconFocused =
            ColorStateList.valueOf(
                ContextCompat.getColor(
                    context,
                    R.color.colorEditTextIconFocused
                )
            )
        mColorEditTextIconDefault =
            ColorStateList.valueOf(
                ContextCompat.getColor(
                    context,
                    R.color.colorEditTextIconDefault
                )
            )

        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.CEditText, 0, 0
        ).apply {

            try {

                if (hasValue(R.styleable.CEditText_hint)) {
                    mTextInputLayout.hint = getString(R.styleable.CEditText_hint)
                }

                if (hasValue(R.styleable.CEditText_secondHint)) {
                    secondHint = getString(R.styleable.CEditText_secondHint)
                }

                if (hasValue(R.styleable.CEditText_text)) {
                    mTextInputEditText.setText(getString(R.styleable.CEditText_text))
                }

                if (hasValue(R.styleable.CEditText_maxLength)) {
                    getString(R.styleable.CEditText_maxLength)?.let {
                        mTextInputEditText.filters =
                            arrayOf<InputFilter>(InputFilter.LengthFilter(it.toInt()))
                    }
                }

                setPasswordVisibilityToggleEnabled(
                    getBoolean(
                        R.styleable.CEditText_passwordToggleEnabled,
                        false
                    )
                )

                if (hasValue(R.styleable.CEditText_startIconDrawable)) {
                    mTextInputLayout.startIconDrawable =
                        getDrawable(R.styleable.CEditText_startIconDrawable)
                }

                if (hasValue(R.styleable.CEditText_endIconTint)) {
                    mEndTintColor = ColorStateList.valueOf(R.styleable.CEditText_endIconTint)
                }

                if (hasValue(R.styleable.CEditText_endIconDrawable)) {
                    mEndDrawable = getDrawable(R.styleable.CEditText_endIconDrawable)
                    setEndDrawableVisibility(
                        getBoolean(
                            R.styleable.CEditText_endIconVisible,
                            false
                        )
                    )
                }

                if (hasValue(R.styleable.CEditText_android_imeOptions)) {
                    mTextInputEditText.imeOptions =
                        getInt(R.styleable.CEditText_android_imeOptions, EditorInfo.IME_NULL)
                }

                if (hasValue(R.styleable.CEditText_android_inputType)) {
                    mTextInputEditText.inputType =
                        getInt(R.styleable.CEditText_android_inputType, EditorInfo.TYPE_NULL)
                }

                mTextInputEditText.setOnFocusChangeListener { v, hasFocus ->
                    mOnFocusChangeListener?.onFocusChange(v, hasFocus)
                    mTextInputLayout.setStartIconTintList(
                        if (hasFocus) mColorEditTextIconFocused else mColorEditTextIconDefault
                    )

                    updateBorderColor()
                }

                setupListener()
            } finally {
                recycle()
            }
        }
    }

    private fun setupListener(){

        mTextInputEditText.afterTextChanged {

            when {
                isSuggestEnabled -> updateSuggestion(this@CEditText.text)
                !secondHint.isNullOrEmpty() -> updateSecondHint(it)
            }

            afterTextChanged?.apply {
                invoke(it)
            }
        }

        mTextInputEditText.setOnFocusChangeListener { _, hasFocus ->

            updateBorderColor()
            when {
                isSuggestEnabled -> autoCompleteSuggestion(this@CEditText.text)
                !secondHint.isNullOrEmpty() -> updateSecondHintVisibility(hasFocus)
            }

            afterFocusChanged?.apply {
                invoke(hasFocus)
            }
        }
    }

    private fun updateBorderColor() {
        if (mErrorTextView.visibility != View.VISIBLE) {
            mBorderView.background = context.getDrawable(
                if (mTextInputEditText.isFocused) R.drawable.bg_cedittext_border_focused else R.drawable.bg_cedittext_border
            )
        } else {
            mBorderView.background = context.getDrawable(R.drawable.bg_cedittext_border_error)
        }
    }

    private fun setPasswordVisibilityToggleEnabled(enabled: Boolean) {
        if (enabled && mTextInputLayout.endIconMode != TextInputLayout.END_ICON_PASSWORD_TOGGLE) {
            mTextInputLayout.endIconMode = TextInputLayout.END_ICON_PASSWORD_TOGGLE
        } else if (!enabled) {
            mTextInputLayout.endIconMode = TextInputLayout.END_ICON_NONE
        }
    }

    override fun setOnFocusChangeListener(l: OnFocusChangeListener) {
        mOnFocusChangeListener = l
    }

    fun setEndDrawableVisibility(visible: Boolean) {
        if (visible && mTextInputLayout.endIconMode != TextInputLayout.END_ICON_CUSTOM) {
            mTextInputLayout.endIconDrawable = mEndDrawable

            if (mEndDrawable != null) {
                if (mEndTintColor != null) {
                    mTextInputLayout.setEndIconTintList(mEndTintColor)
                    mTextInputLayout.setEndIconTintMode(PorterDuff.Mode.ADD)
                }
                mTextInputLayout.endIconMode = TextInputLayout.END_ICON_CUSTOM
            } else {
                mTextInputLayout.endIconMode = TextInputLayout.END_ICON_NONE
            }
        } else if (!visible) {
            mTextInputLayout.endIconMode = TextInputLayout.END_ICON_NONE
        }
    }

    fun setEndIconDrawable(drawable: Drawable?) {
        mEndDrawable = drawable
        setEndDrawableVisibility(true)
    }

    fun setEndIconTintList(color: ColorStateList) {
        mEndTintColor = color
        setEndDrawableVisibility(true)
    }

    fun requestInputFocus() {
        mTextInputEditText.requestFocus()
    }

    private fun updateSecondHintVisibility(hasFocus: Boolean) {
        secondHint?.apply {
            val length = mTextInputEditText.text?.length ?: 0
            mSuggestTextView.text = when (hasFocus) {
                true -> when (length == secondHintMax) {
                    true -> ""
                    false -> this
                }
                false -> when (length in 1..secondHintMax) {
                    true -> ""
                    false -> ""
                }
            }
        }
    }

    private fun updateSecondHint(text: String) {
        secondHint?.apply {
            val value = mTextInputEditText.text ?: ""
            if (value.length > length) {
                mSuggestTextView.text = value
            } else {
                mSuggestTextView.text = value.toString() + substring(value.length, length)
            }
        }
    }

    fun setOnEditorActionListener(l: (actionId: Int, event: KeyEvent?) -> Boolean) {
        mTextInputEditText.setOnEditorActionListener { _, actionId, event ->
            l.invoke(actionId, event)
        }
    }

    fun setSuggestions(suggestions: List<String>, delimiter: String) {
        this.suggestionList = suggestions
        this.delimiter = delimiter
    }

    private fun updateSuggestion(email: String) {

        val domain = email.substringAfter(this.delimiter, "")
        val address = email.substringBefore(this.delimiter, "")
        var isOnSuggestionList = false

        if (domain.isNotEmpty() && address.isNotEmpty() && suggestionList.isNotEmpty()) {

            for (suggestion in suggestionList) {

                if (domain.first() == suggestion.first()) {

                    if (domain.length <= suggestion.length) {

                        val pref = suggestion.removeRange(0, domain.length)
                        val sub = domain.replace(domain, pref)

                        if (!suggestion.contains(domain)) {

                            mSuggestTextView.text = ""
                        } else {
                            isOnSuggestionList = true
                            mSuggestTextView.text = "$email$sub"
                        }
                    }
                }
            }
            if (!isOnSuggestionList) {
                mSuggestTextView.text = ""
            }
        } else if (email.contains(this.delimiter) && domain.isEmpty() && address.isNotEmpty() && suggestionList.isNotEmpty()) {
            mSuggestTextView.text = "$email${suggestionList.first()}"
        } else {
            mSuggestTextView.text = ""
        }
    }

    private fun autoCompleteSuggestion(email: String) {

        val domain = email.substringAfter(this.delimiter, "")

        if (domain.isNotEmpty() && suggestionList.isNotEmpty()) {

            for (suggestion in suggestionList) {

                if (domain.first() == suggestion.first()) {

                    if (domain.length <= suggestion.length) {
                        val pref = suggestion.removeRange(0, domain.length)
                        val sub = domain.replace(domain, pref)
                        mTextInputEditText.setText("$email$sub")
                    }
                }
            }
        } else if (email.contains(this.delimiter) && domain.isEmpty() && suggestionList.isNotEmpty()) {
            mTextInputEditText.setText("$email${suggestionList.first()}")
        }
    }

    fun setFilters(filters: Array<InputFilter>) {
        mTextInputEditText.filters = filters
    }

    override fun setOnClickListener(l: OnClickListener?) {
        mTouchView.setOnClickListener(l)
        mTouchView.visibility = when(l){
            null -> View.GONE
            else -> View.VISIBLE
        }
    }
}