package com.tdp2.utils.ui.placepicker

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.tdp2.utils.extensions.append
import com.tdp2.utils.extensions.checkPermissionForLocation
import com.tdp2.utils.extensions.hideKeyboard
import com.tdp2.utils.extensions.requestPermissionForLocation
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.tdp2.utils.R
import kotlinx.android.synthetic.main.activity_location_picker.*
import java.io.IOException


class LocationPickerActivity : AppCompatActivity() {

    companion object {
        const val SELECTED_PLACE = "place"
        const val API_KEY = "api_key"

        @JvmStatic
        fun newIntent(context: Context, googleApiKey: String): Intent {
            val intent = Intent(context, LocationPickerActivity::class.java)
            intent.putExtra(API_KEY, googleApiKey)
            return intent
        }
    }

    private val REQUEST_PERMISSIONS_REQUEST_CODE = 1001
    private val DEFAULT_ZOOM = 15.0f
    private var googleMap: GoogleMap? = null
    private var selectedMarker: Marker? = null
    private var firstTime = true
    private var placesApi: PlaceAPI? = null
    private lateinit var apiKey: String
    private var mFusedLocationProviderClient: FusedLocationProviderClient? = null
    private var mLastKnownLocation: Location? = null

    var selectedPlace: Place? = null
        set(value) {
            field = value
            this.runOnUiThread {
                value?.getCoordinates()?.apply {
                    subtitleTextView.text = value.addressDetail
                    locationAutoComplete.setText(value.addressDetail)
                    addPin(this)
                    popupView.visibility = View.VISIBLE
                    locationAutoComplete.dismissDropDown()
                } ?: run {
                    subtitleTextView.text = ""
                    popupView.visibility = View.GONE
                    locationAutoComplete.setText("")
                    googleMap?.let {
                        selectedMarker?.remove()
                    }
                }
            }
        }

    private var autocompleteAdapter: PlacesAutoCompleteAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location_picker)
        setTitle(R.string.place_picker_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        intent.getStringExtra(API_KEY)?.apply {
            apiKey = this
        } ?: run {
            finish()
        }

        if (apiKey.isEmpty()) {
            Toast.makeText(this, "Agregue el APIKEY en el build.gradle", Toast.LENGTH_LONG).show()
        }

        mapView.onCreate(savedInstanceState)
        mapView.onResume()
        MapsInitializer.initialize(this)

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        setupListeners()

        Handler().postDelayed({
            toastView.visibility = View.GONE
        }, 3000)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            mapView.onDestroy()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    private fun setupListeners() {

        mapView.getMapAsync {
            googleMap = it

            showCurrentLocation()

            if (firstTime) {
                gotoMyLocation()
                firstTime = false
            }

            it.setOnMapLongClickListener { latLng ->
                getPlace(latLng)
            }

        }

        floatingActionButton.setOnClickListener {
            gotoMyLocation()
        }

        locationAutoComplete.setOnClickListener {
            if (locationAutoComplete.text.isNotEmpty())
                locationAutoComplete.showDropDown()
        }

        locationAutoComplete.setOnEditorActionListener { _, actionId, _ ->
            when (actionId) {
                EditorInfo.IME_ACTION_SEARCH -> {
                    autocompleteAdapter?.resultList?.let {
                        if (it.isNotEmpty()) {
                            val place = it.first()
                            locationAutoComplete.setText(place.addressDetail)
                            getPlaceDetails(place)
                            locationAutoComplete.dismissDropDown()
                            hideKeyboard()
                        }
                    }
                }
            }
            false
        }

        placesApi = PlaceAPI.Builder()
            .apiKey(apiKey)
            .build(this).apply {
                autocompleteAdapter = PlacesAutoCompleteAdapter(this@LocationPickerActivity, this)
                locationAutoComplete.setAdapter(autocompleteAdapter)
                locationAutoComplete.onItemClickListener =
                    AdapterView.OnItemClickListener { parent, _, position, _ ->
                        val place = parent.getItemAtPosition(position) as Place
                        locationAutoComplete.setText(place.addressDetail)
                        getPlaceDetails(place)
                        hideKeyboard()
                    }
            }

        popupView.setOnClickListener {
            val intent = Intent()
            selectedPlace?.apply {
                val bundle = Bundle()
                bundle.putSerializable(SELECTED_PLACE, this)
                intent.putExtras(bundle)
                setResult(RESULT_OK, intent)
            }
            finish()
        }
    }

    private fun showCurrentLocation() {

        if (!checkPermissionForLocation()) {
            requestPermissionForLocation(REQUEST_PERMISSIONS_REQUEST_CODE)
            return
        }

        googleMap?.apply {
            isMyLocationEnabled = true
            getDeviceLocation()
        }
    }

    private fun gotoMyLocation() {
        showCurrentLocation()
    }

    private fun getDeviceLocation() {
        try {
            val locationResult = mFusedLocationProviderClient?.lastLocation
            locationResult?.addOnCompleteListener {
                if (it.isSuccessful) {
                    mLastKnownLocation = it.result?.apply {
                        googleMap?.moveCamera(
                            CameraUpdateFactory.newLatLngZoom(
                                LatLng(latitude, longitude), DEFAULT_ZOOM
                            )
                        )
                    }
                }
            }
        } catch (e: SecurityException) {

        }
    }

    private fun getPlace(latLng: LatLng) {

        try {
            val addresses = Geocoder(this).getFromLocation(latLng.latitude, latLng.longitude, 1)
            if (null != addresses && addresses.isNotEmpty()) {
                val place = addresses[0]
                selectedPlace = Place().apply {
                    setCoordinate(latLng)
                    this.address = place.featureName.append(place.thoroughfare, " ")
                    city = place.subLocality
                    state = place.adminArea
                    country = place.countryName
                    zipcode = place.postalCode
                }
            } else {
                selectedPlace = null
            }
        } catch (e: IOException) {
            placesApi?.getFromLocation(latLng.latitude, latLng.longitude, object :
                OnAddressListener {
                override fun onError(errorMessage: String) {
                    selectedPlace = null
                }

                override fun onAddressFetched(address: Address) {
                    val place = Place()
                    place.setCoordinate(latLng)
                    place.address = address.featureName.append(address.thoroughfare, " ")
                    place.city = address.subLocality
                    place.state = address.adminArea
                    place.country = address.countryName
                    place.zipcode = address.postalCode
                    selectedPlace = place
                }
            })
        }

    }

    private fun getPlaceDetails(place: Place) {
        placesApi?.fetchPlaceDetails(place, object :
            OnPlacesDetailsListener {
            override fun onError(errorMessage: String) {
            }

            override fun onPlaceFetched(place: Place) {
                if (place.getCoordinates() != null) {
                    selectedPlace = place
                    this@LocationPickerActivity.runOnUiThread {
                        googleMap?.apply {
                            moveCamera(
                                CameraUpdateFactory.newLatLngZoom(
                                    place.getCoordinates()!!,
                                    15.0f
                                )
                            )
                        }
                    }
                } else {
                    selectedPlace = null
                }
            }
        })
    }

    private fun addPin(latLng: LatLng) {
        this.runOnUiThread {
            googleMap?.let {
                selectedMarker?.remove()
                val iconMarker = BitmapDescriptorFactory.fromResource(R.drawable.ic_map_pin)
                selectedMarker = it.addMarker(
                    MarkerOptions()
                        .position(latLng).icon(iconMarker)
                )
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.isEmpty()) {
                Log.i(this.javaClass.name, "User interaction was cancelled.")
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                showCurrentLocation()
            }
        }
    }
}
