package com.tdp2.utils.ui.layout

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import com.tdp2.utils.R

class RectangleLayout : ConstraintLayout {

    private var aspectRatio: Float = 1.35f

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initialize(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initialize(context, attrs)
    }

    private fun initialize(context: Context, attrs: AttributeSet?) {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.RectangleLayout, 0, 0
        ).apply {

            try {

                if (hasValue(R.styleable.RectangleLayout_aspectRatio)) {
                    aspectRatio = getFloat(R.styleable.RectangleLayout_aspectRatio, 1.35f)
                }

            } finally {
                recycle()
            }
        }
    }

    public override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = MeasureSpec.getSize(widthMeasureSpec)
        val height = kotlin.math.ceil((width * aspectRatio).toDouble())

        val measureHeight = MeasureSpec.makeMeasureSpec(
            height.toInt(),
            MeasureSpec.EXACTLY
        )

        super.onMeasure(widthMeasureSpec, measureHeight)
        setMeasuredDimension(widthMeasureSpec, measureHeight)
    }

}