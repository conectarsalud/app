package com.tdp2.conectarsalud.presentation.ui.videocall

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.tdp2.conectarsalud.R
import com.tdp2.conectarsalud.data.local.model.Appointment
import com.tdp2.conectarsalud.data.mapper.toAppointment
import com.tdp2.conectarsalud.data.remote.utils.NetworkConnectionException
import com.tdp2.conectarsalud.data.remote.utils.Result
import com.tdp2.conectarsalud.data.repository.HealthRepository
import com.tdp2.conectarsalud.presentation.ui.base.BaseViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

class RatingViewModel
@Inject constructor(private val healthRepository: HealthRepository) : BaseViewModel() {

    private val TAG: String = "RATING_VM"

    private val mRateAppointment = MutableLiveData<Boolean>()
    val rating: LiveData<Boolean>
        get() = mRateAppointment

    fun makeWarningDialog(){
        showWarningDialog(R.string.rating_warning)
    }

    fun rateAppointment(appointmentId: String, rating: Int, comments: String? = null) {

        mDataLoading.value = true
        viewModelScope.launch {
            when (val result = healthRepository.rateAppointment(appointmentId, rating, comments)) {
                // Successful HTTP result
                is Result.Ok -> {
                    mRateAppointment.value = true
                }
                // Any HTTP error
                is Result.Error -> {
                    showErrorDialog(R.string.loading_error)
                }
                // Exception while request invocation
                is Result.Exception -> {
                    when (result.exception) {
                        is NetworkConnectionException -> showErrorDialog(R.string.connection_error)
                        else -> showErrorDialog(R.string.loading_error)
                    }
                }
            }
            mDataLoading.value = false
        }
    }
}