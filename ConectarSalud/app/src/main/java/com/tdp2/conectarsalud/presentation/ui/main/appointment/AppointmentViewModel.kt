package com.tdp2.conectarsalud.presentation.ui.main.appointment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.tdp2.conectarsalud.R
import com.tdp2.conectarsalud.data.local.model.Appointment
import com.tdp2.conectarsalud.data.local.model.Specialty
import com.tdp2.conectarsalud.data.mapper.toAppointment
import com.tdp2.conectarsalud.data.mapper.toSpecialty
import com.tdp2.conectarsalud.data.remote.utils.NetworkConnectionException
import com.tdp2.conectarsalud.data.remote.utils.Result
import com.tdp2.conectarsalud.data.repository.HealthRepository
import com.tdp2.conectarsalud.presentation.ui.base.BaseViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

class AppointmentViewModel
@Inject constructor(private val healthRepository: HealthRepository) : BaseViewModel() {

    private val TAG: String = "APPOINTMENT_VM"

    private val mSpecialties = MutableLiveData<List<Specialty>>()
    val specialties: LiveData<List<Specialty>> = mSpecialties
    private val mNewAppointment = MutableLiveData<Appointment>()
    val appointment: LiveData<Appointment> = mNewAppointment

    fun setAppointment(appointment: Appointment) {
        healthRepository.setAppointment(appointment)
    }

    fun getSpecialties() {

        mDataLoading.value = true

        viewModelScope.launch {
            when (val result = healthRepository.getSpecialties()) {
                // Successful HTTP result
                is Result.Ok -> {
                    mSpecialties.value = result.value.specialties.map { it.toSpecialty() }
                }
                // Any HTTP error
                is Result.Error -> {
                    showErrorDialog(R.string.loading_error)
                }
                // Exception while request invocation
                is Result.Exception -> {
                    when (result.exception) {
                        is NetworkConnectionException -> showErrorDialog(R.string.connection_error)
                        else -> showErrorDialog(R.string.loading_error)
                    }
                }
            }
            mDataLoading.value = false
        }
    }

    fun newAppointment(specialtyId: Int, reason: String) {

        mDataLoading.value = true
        viewModelScope.launch {
            when (val result = healthRepository.newAppointment(specialtyId, reason)) {
                // Successful HTTP result
                is Result.Ok -> {
                    mNewAppointment.value = result.value.appointment.toAppointment()
                }
                // Any HTTP error
                is Result.Error -> {
                    showErrorDialog(R.string.loading_error)
                }
                // Exception while request invocation
                is Result.Exception -> {
                    when (result.exception) {
                        is NetworkConnectionException -> showErrorDialog(R.string.connection_error)
                        else -> showErrorDialog(R.string.loading_error)
                    }
                }
            }
            mDataLoading.value = false
        }
    }
}