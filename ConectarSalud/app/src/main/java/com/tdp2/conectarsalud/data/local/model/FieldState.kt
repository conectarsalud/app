package com.tdp2.conectarsalud.data.local.model

import androidx.annotation.StringRes

class FieldState(
    @StringRes var error: Int? = null,
    var isValid: Boolean = false
)