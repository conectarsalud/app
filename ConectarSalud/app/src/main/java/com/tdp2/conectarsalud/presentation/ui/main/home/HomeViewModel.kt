package com.tdp2.conectarsalud.presentation.ui.main.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.tdp2.conectarsalud.R
import com.tdp2.conectarsalud.data.local.model.Appointment
import com.tdp2.conectarsalud.data.mapper.toAppointment
import com.tdp2.conectarsalud.data.remote.utils.NetworkConnectionException
import com.tdp2.conectarsalud.data.remote.utils.Result
import com.tdp2.conectarsalud.data.repository.HealthRepository
import com.tdp2.conectarsalud.presentation.ui.base.BaseViewModel
import com.tdp2.conectarsalud.utils.Constants
import kotlinx.coroutines.launch
import javax.inject.Inject

class HomeViewModel
@Inject constructor(private val healthRepository: HealthRepository) : BaseViewModel() {

    private val TAG: String = "HOME_VM"

    private val mCancelAppointment = MutableLiveData<Appointment>()
    val cancelAppointment: LiveData<Appointment>
        get() = mCancelAppointment

    fun logout() {
        healthRepository.logout()
    }

    fun hasAppointment(): Boolean {
        return healthRepository.hasAppointment()
    }

    fun getAppointment(): Appointment {
        return healthRepository.getAppointment()
    }

    fun clearAppointment() {
        healthRepository.clearAppointment()
    }

    fun registerToPushNotifications(token: String) {
        viewModelScope.launch {
            when (val result = healthRepository.registerDevice(token)) {
                // Successful HTTP result
                is Result.Ok -> {
                    Log.d(TAG, "Token sent to server: $token")
                }
                // Any HTTP error
                is Result.Error -> {
                    //showErrorDialog(R.string.loading_error)
                    Log.e(TAG, "Error")
                }
                // Exception while request invocation
                is Result.Exception -> {
                    /*when (result.exception) {
                        is NetworkConnectionException -> showErrorDialog(R.string.connection_error)
                        else -> showErrorDialog(R.string.loading_error)
                    }*/
                    result.exception.message?.apply {
                        Log.e(TAG, this)
                    }
                }
            }
        }
    }

    fun cancelAppointment(appointmentId: String) {

        mDataLoading.value = true
        viewModelScope.launch {
            when (val result = healthRepository.cancelAppointment(appointmentId)) {
                // Successful HTTP result
                is Result.Ok -> {
                    mCancelAppointment.value = result.value.appointment.toAppointment()
                }
                // Any HTTP error
                is Result.Error -> {
                    when (result.exception.code()) {
                        Constants.StatusCode.INVALID_APPOINTMENT_ACTION -> showWarningDialog(R.string.invalid_appointment_cancel)
                        else -> showErrorDialog(R.string.loading_error)
                    }
                }
                // Exception while request invocation
                is Result.Exception -> {
                    when (result.exception) {
                        is NetworkConnectionException -> showErrorDialog(R.string.connection_error)
                        else -> showErrorDialog(R.string.loading_error)
                    }
                }
            }
            mDataLoading.value = false
        }
    }
}