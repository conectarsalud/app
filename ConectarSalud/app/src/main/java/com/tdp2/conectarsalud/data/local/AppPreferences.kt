package com.tdp2.conectarsalud.data.local

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.GsonBuilder
import com.tdp2.conectarsalud.data.local.model.Appointment
import java.util.*

interface Session {
    var authToken: String?
    var lastUpdate: Long
    var timestamp: Long
    fun clearSession()
    fun hasSession(): Boolean
}

object AppPreferences : Session {

    private const val NAME = "ConectarSalud"
    private const val MODE = Context.MODE_PRIVATE
    private lateinit var preferences: SharedPreferences

    private const val AUTH_TOKEN_KEY = "AUTH_TOKEN_v1"
    private const val AUTH_TOKEN_EXPIRATION_KEY = "AUTH_TOKEN_EXPIRATION_v1"
    private const val TIMESTAMP_KEY = "TIMESTAMP_v1"
    private const val APPOINTMENT = "APPOINTMENT"
    private const val expiredTokenTime: Long = 1000L * 12 * 60 * 60 // 12 hs

    fun init(context: Context) {
        preferences = context.getSharedPreferences(
            NAME,
            MODE
        )
    }

    var appointment: Appointment?
        get(): Appointment? {
            val value = preferences.getString(APPOINTMENT, null)
            return GsonBuilder().create().fromJson(value, Appointment::class.java)
        }
        set(value) {
            val editor = preferences.edit()
            val jsonString = GsonBuilder().create().toJson(value)
            editor.putString(APPOINTMENT, jsonString)
            editor.apply()
        }

    fun clearAppointment() {
        this.appointment = null
    }

    override var authToken: String?
        get(): String? {
            return preferences.getString(AUTH_TOKEN_KEY, null)
        }
        set(value) {
            val editor = preferences.edit()
            editor.putString(AUTH_TOKEN_KEY, value)
            editor.apply()
        }

    override var lastUpdate: Long
        get(): Long {
            return preferences.getLong(AUTH_TOKEN_EXPIRATION_KEY, 0L)
        }
        set(value) {
            val editor = preferences.edit()
            editor.putLong(AUTH_TOKEN_EXPIRATION_KEY, value)
            editor.apply()
        }

    override var timestamp: Long
        get(): Long {
            return preferences.getLong(TIMESTAMP_KEY, 0L)
        }
        set(value) {
            val editor = preferences.edit()
            editor.putLong(TIMESTAMP_KEY, value)
            editor.apply()
        }

    override fun hasSession(): Boolean {
        return !authToken.isNullOrEmpty() && lastUpdate != 0L && Date().time - lastUpdate < expiredTokenTime
    }

    override fun clearSession() {
        this.authToken = null
    }
}