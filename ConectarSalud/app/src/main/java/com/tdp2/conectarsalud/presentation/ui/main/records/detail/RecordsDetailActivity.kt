package com.tdp2.conectarsalud.presentation.ui.main.records.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.tdp2.conectarsalud.R
import com.tdp2.conectarsalud.data.local.model.Record
import com.tdp2.conectarsalud.presentation.ui.base.BaseActivity
import com.tdp2.conectarsalud.utils.Constants
import com.tdp2.utils.extensions.*
import com.tdp2.utils.ui.decorator.HorizontalSpaceItemDecoration
import com.tdp2.utils.ui.imageViewer.ImageViewerActivity
import kotlinx.android.synthetic.main.activity_records_detail.*
import javax.inject.Inject

class RecordsDetailActivity : BaseActivity() {

    companion object {
        fun newIntent(context: Context, record: Record): Intent {
            val intent = Intent(context, RecordsDetailActivity::class.java)
            intent.putExtra(Constants.RECORD, record)
            return intent
        }

        private const val TAG = "RECORDS_DETAIL_ACT"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var recordsDetailViewModel: RecordsDetailViewModel
    private lateinit var mRecord: Record
    private lateinit var prescriptionAdapter: PrescriptionAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_records_detail)

        recordsDetailViewModel =
            ViewModelProvider(this, viewModelFactory).get(RecordsDetailViewModel::class.java)

        intent.getParcelableExtra<Record>(Constants.RECORD)?.apply {
            mRecord = this
        }

        init()
        setupListeners()
        setupObservers()
    }

    private fun init() {
        val date = mRecord.date.split(" ").first()
        setupToolbar(
            toolbar,
            "${resources.getString(R.string.records_detail)} $date"
        )
        setSupportActionBar(toolbar)
        addBackButton(toolbar)

        specialtyTextView.text = mRecord.specialty
        doctorTextView.text = mRecord.doctor.name
        reasonTextView.text = mRecord.reason
        mRecord.indications?.let {
            indicationsTitle.visibility = View.VISIBLE
            indicationsTextView.text = it
            indicationsTextView.visibility = View.VISIBLE
        }
        prescriptionAdapter = PrescriptionAdapter { element, view ->
            val intent = ImageViewerActivity.newIntent(this, element)
            startActivity(intent)
        }
        mRecord.prescriptions?.let {
            prescriptionsTitle.visibility = View.VISIBLE
            prescriptionsRecyclerView.visibility = View.VISIBLE
            prescriptionsRecyclerView.addItemDecoration(HorizontalSpaceItemDecoration(8.px))
            prescriptionsRecyclerView.layoutManager =
                LinearLayoutManager(
                    this@RecordsDetailActivity,
                    LinearLayoutManager.HORIZONTAL,
                    false
                )
            prescriptionsRecyclerView.adapter = prescriptionAdapter
            prescriptionAdapter.refresh(it)
        }
    }

    private fun setupListeners() {

    }

    private fun setupObservers() {

        recordsDetailViewModel.dataLoading.observe(this@RecordsDetailActivity, Observer { loading ->
            when (loading) {
                true -> loadingContentView.visible(true)
                false -> loadingContentView.gone(true)
            }
        })

        mainView.setupSnackbar(
            this@RecordsDetailActivity,
            recordsDetailViewModel.snackBarMessage,
            Snackbar.LENGTH_LONG
        )
        mainView.setupToast(
            this@RecordsDetailActivity,
            recordsDetailViewModel.toastMessage,
            Toast.LENGTH_LONG
        )
        mainView.setupErrorDialog(this@RecordsDetailActivity, recordsDetailViewModel.errorDialog)
    }
}