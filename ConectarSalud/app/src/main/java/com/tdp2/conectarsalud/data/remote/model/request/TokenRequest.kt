package com.tdp2.conectarsalud.data.remote.model.request

class TokenRequest (val device_token: String)