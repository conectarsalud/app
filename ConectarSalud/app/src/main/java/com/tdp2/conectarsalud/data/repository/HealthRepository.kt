package com.tdp2.conectarsalud.data.repository

import android.content.Context
import com.tdp2.conectarsalud.R
import com.tdp2.conectarsalud.data.local.AppPreferences
import com.tdp2.conectarsalud.data.local.model.Appointment
import com.tdp2.conectarsalud.data.remote.ApiClient
import com.tdp2.conectarsalud.data.remote.model.dto.*
import com.tdp2.conectarsalud.data.remote.model.request.AppointmentRequest
import com.tdp2.conectarsalud.data.remote.model.request.LoginRequest
import com.tdp2.conectarsalud.data.remote.model.request.RatingRequest
import com.tdp2.conectarsalud.data.remote.model.request.TokenRequest
import com.tdp2.conectarsalud.data.remote.utils.*
import com.tdp2.conectarsalud.presentation.di.base.ApplicationContext
import okhttp3.Response
import java.util.*
import javax.inject.Inject

class HealthRepository @Inject constructor(
    private val networkHandler: NetworkHandler,
    private val services: ApiClient,
    @ApplicationContext val context: Context
) {

    private var hospitalsDataDto: HospitalsDataDto? = null
    private var lastUpdate = 0L
    private val expiredHospitalsData: Long = 1000L * 60 * 60 // 60 min

    suspend fun getHospitalsData(): Result<HospitalsDataDto> {

        if (Date().time - lastUpdate > expiredHospitalsData) {
            hospitalsDataDto = null
        }
        hospitalsDataDto?.apply {
            return Result.Ok(this, Response.Builder().buildSuccess())
        }

        return when (networkHandler.isConnected) {
            true -> {
                val result = services.getHospitalsData().awaitResult()
                when (result) {
                    is Result.Ok -> {
                        hospitalsDataDto = result.value
                        lastUpdate = Date().time
                    }
                }
                result
            }
            false, null -> Result.Exception(NetworkConnectionException())
        }
    }

    suspend fun getSpecialties(): Result<SpecialtiesDto> {
        return when (networkHandler.isConnected) {
            true -> {
                services.getSpecialties().awaitResult()
            }
            false, null -> Result.Exception(NetworkConnectionException())
        }
    }

    fun isUserLogged(): Boolean {
        return AppPreferences.hasSession()
    }

    fun hasAppointment(): Boolean {
        return AppPreferences.appointment != null
    }

    fun getAppointment(): Appointment {
        return AppPreferences.appointment!!
    }

    fun setAppointment(appointment: Appointment) {
        AppPreferences.appointment = appointment
    }

    fun clearAppointment() {
        AppPreferences.clearAppointment()
    }

    fun logout() {
        AppPreferences.clearSession()
    }

    suspend fun login(username: String, password: String): Result<LoginDto> {
        val request = LoginRequest(username, password)
        return when (networkHandler.isConnected) {
            true -> {
                val result = services.login(request).awaitResult()
                when (result) {
                    is Result.Ok -> {
                        context.resources
                        AppPreferences.authToken =
                            "${context.resources.getString(R.string.token)} ${result.value.token.authToken}"
                        val timestamp = result.value.token.tokenExpiration
                        if (timestamp != AppPreferences.timestamp) {
                            AppPreferences.timestamp = timestamp
                            AppPreferences.lastUpdate = Date().time
                        }
                    }
                }
                result
            }
            false, null -> Result.Exception(NetworkConnectionException())
        }
    }

    suspend fun newAppointment(specialtyId: Int, reason: String): Result<NewAppointmentDto> {
        val request = AppointmentRequest(specialtyId, reason)
        return when (networkHandler.isConnected) {
            true -> {
                services.newAppointment(request).awaitResult()
            }
            false, null -> Result.Exception(NetworkConnectionException())
        }
    }

    suspend fun startAppointment(appointmentId: String): Result<NewAppointmentDto> {
        return when (networkHandler.isConnected) {
            true -> {
                services.startAppointment(appointmentId).awaitResult()
            }
            false, null -> Result.Exception(NetworkConnectionException())
        }
    }

    suspend fun finishAppointment(appointmentId: String): Result<NewAppointmentDto> {
        return when (networkHandler.isConnected) {
            true -> {
                services.finishAppointment(appointmentId).awaitResult()
            }
            false, null -> Result.Exception(NetworkConnectionException())
        }
    }

    suspend fun cancelAppointment(appointmentId: String): Result<NewAppointmentDto> {
        return when (networkHandler.isConnected) {
            true -> {
                services.cancelAppointment(appointmentId).awaitResult()
            }
            false, null -> Result.Exception(NetworkConnectionException())
        }
    }

    suspend fun rateAppointment(
        appointmentId: String,
        rating: Int,
        comments: String?
    ): Result<RatingAppointmentDto> {
        val request = RatingRequest(rating, comments)
        return when (networkHandler.isConnected) {
            true -> {
                services.rateAppointment(appointmentId, request).awaitResult()
            }
            false, null -> Result.Exception(NetworkConnectionException())
        }
    }

    suspend fun getRecords(): Result<RecordsDto> {
        return when (networkHandler.isConnected) {
            true -> {
                services.getRecords().awaitResult()
            }
            false, null -> Result.Exception(NetworkConnectionException())
        }
    }

    suspend fun getQueue(appointmentId: String): Result<NewAppointmentDto> {
        return when (networkHandler.isConnected) {
            true -> {
                services.getQueue(appointmentId).awaitResult()
            }
            false, null -> Result.Exception(NetworkConnectionException())
        }
    }

    suspend fun registerDevice(token: String): Result<NotificationDto> {
        val request = TokenRequest(token)
        return when (networkHandler.isConnected) {
            true -> {
                services.registerDevice(request).awaitResult()
            }
            false, null -> Result.Exception(NetworkConnectionException())
        }
    }
}