package com.tdp2.conectarsalud.presentation.ui.base

import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tdp2.conectarsalud.utils.AppApplication
import com.tdp2.utils.general.Event

open class BaseViewModel : ViewModel() {

    protected val mDataLoading = MutableLiveData<Boolean>()
    val dataLoading: LiveData<Boolean>
        get() = mDataLoading

    private val mSnackBarText = MutableLiveData<Event<String>>()
    val snackBarMessage: LiveData<Event<String>>
        get() = mSnackBarText

    private val mToastText = MutableLiveData<Event<Int>>()
    val toastMessage: LiveData<Event<Int>>
        get() = mToastText

    private val mErrorDialog = MutableLiveData<Event<Int>>()
    val errorDialog: LiveData<Event<Int>>
        get() = mErrorDialog

    private val mWarningDialog = MutableLiveData<Event<Int>>()
    val warningDialog: LiveData<Event<Int>>
        get() = mWarningDialog

    protected val mShowLoginScreen = MutableLiveData<Event<Boolean>>()
    val showLoginScreen: LiveData<Event<Boolean>>
        get() = mShowLoginScreen

    protected fun showSnackbarMessage(message: String) {
        mSnackBarText.value = Event(message)
    }

    protected fun showSnackbarMessage(@StringRes resource: Int) {
        showSnackbarMessage(AppApplication.instance!!.getString(resource))
    }

    protected fun showToastMessage(message: String) {
        mSnackBarText.value = Event(message)
    }

    protected fun showToastMessage(@StringRes resource: Int) {
        showToastMessage(AppApplication.instance!!.getString(resource))
    }

    protected fun showErrorDialog(@StringRes message: Int) {
        mErrorDialog.value = Event(message)
    }

    protected fun showWarningDialog(@StringRes message: Int) {
        mWarningDialog.value = Event(message)
    }

}