package com.tdp2.conectarsalud.data.remote.utils

import java.lang.Exception

class NetworkConnectionException: Exception("Network Connection Error")