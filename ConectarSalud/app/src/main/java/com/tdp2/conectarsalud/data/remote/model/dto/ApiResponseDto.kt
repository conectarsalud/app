package com.tdp2.conectarsalud.data.remote.model.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ApiResponseDto(
    @Json(name = "code") val code: String,
    @Json(name = "message") val message: String
)

@JsonClass(generateAdapter = true)
data class LoginDto(
    @Json(name = "token") val token: TokenDto
)

@JsonClass(generateAdapter = true)
data class TokenDto(
    @Json(name = "token") val authToken: String,
    @Json(name = "tokenExpiration") val tokenExpiration: Long
)