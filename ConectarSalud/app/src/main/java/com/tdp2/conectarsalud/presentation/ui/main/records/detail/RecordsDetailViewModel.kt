package com.tdp2.conectarsalud.presentation.ui.main.records.detail

import com.tdp2.conectarsalud.data.repository.HealthRepository
import com.tdp2.conectarsalud.presentation.ui.base.BaseViewModel
import javax.inject.Inject

class RecordsDetailViewModel
@Inject constructor(private val healthRepository: HealthRepository) : BaseViewModel() {

    private val TAG: String = "RECORDS_DETAIL_VM"
}