package com.tdp2.conectarsalud.data.remote.model.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class HospitalsDataDto(
    @Json(name = "hospitals") val hospitals: List<HospitalDto>
)

@JsonClass(generateAdapter = true)
data class HospitalDto(
    @Json(name = "id") val id: String,
    @Json(name = "nombre") val name: String,
    @Json(name = "calle_altura") val address: String,
    @Json(name = "telefono") val phoneNumber: String,
    @Json(name = "guardia") val guardPhoneNumber: String,
    @Json(name = "tipo_espec") val specialty: String,
    @Json(name = "barrio") val neighborhood : String,
    @Json(name = "comuna") val district: String,
    @Json(name = "lat") val lat: String,
    @Json(name = "long") val long: String
)