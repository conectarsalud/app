package com.tdp2.conectarsalud.data.local.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Appointment(
    val id: String,
    val doctorId: String,
    val state: AppointmentState,
    val channelId: String?,
    val queue: String?
) : Parcelable

@Parcelize
data class Record(
    val id: Int,
    val specialty: String,
    val doctor: Doctor,
    val reason: String,
    val date: String,
    val indications: String?,
    val prescriptions: List<String>?
) : Parcelable

@Parcelize
data class Doctor(
    val id: Int,
    val name: String
) : Parcelable

@Parcelize
data class RecordNotification(
    val id: Int,
    val especialidad: String,
    val member_id: Int,
    val doctor_id: Int,
    val doctor_nombre: String,
    val estado: String,
    val motivo: String,
    val hora_solicitud: String,
    val informacion: RecordInformation
) : Parcelable

@Parcelize
data class RecordInformation(
    val indicaciones: String?,
    val recetas: List<String>?
) : Parcelable

fun RecordNotification.toRecord(): Record {
    return Record(
        id = this.id,
        specialty = this.especialidad,
        doctor = Doctor(this.doctor_id, this.doctor_nombre),
        reason = this.motivo,
        date = this.hora_solicitud,
        indications = this.informacion.indicaciones,
        prescriptions = this.informacion.recetas
    )
}