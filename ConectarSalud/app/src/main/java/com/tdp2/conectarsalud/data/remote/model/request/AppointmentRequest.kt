package com.tdp2.conectarsalud.data.remote.model.request

class AppointmentRequest (val medical_specialty_id: Int, val reason: String)