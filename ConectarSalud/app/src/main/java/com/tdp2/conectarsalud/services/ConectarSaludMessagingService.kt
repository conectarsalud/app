package com.tdp2.conectarsalud.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.tdp2.conectarsalud.R
import com.tdp2.conectarsalud.presentation.ui.main.home.HomeActivity
import com.tdp2.conectarsalud.utils.AppApplication
import com.tdp2.conectarsalud.utils.Constants

class ConectarSaludMessagingService : FirebaseMessagingService() {

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        var message = ""
        var title = ""
        var open = ""
        var open2 = ""

        // Check if message contains a data payload.
        remoteMessage.data.let {
            message = it["default"] ?: ""
        }

        // Check if message contains a notification payload.
        remoteMessage.notification?.let {
            message = it.body ?: ""
            title = it.title ?: getString(R.string.app_name)
        }

        remoteMessage.data.let {
            open = it[Constants.PushNotifications.APPOINTMENT] ?: ""
            open2 = it[Constants.PushNotifications.RECORD] ?: ""
        }

        if (AppApplication.instance?.isAppInBackground == true) {
            sendNotification(message, title, open, open2)
        } else {
            sendNotification(message, title, open, open2)
            sendBroadCast(this, message)
        }
    }
    // [END receive_message]

    // [START on_new_token]
    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    override fun onNewToken(token: String) {
        Log.d("APPXX", "Refreshed token: $token")
        //TODO: enviar token al server
    }
    // [END on_new_token]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private fun sendNotification(messageBody: String, title: String, open: String, open2: String) {

        val intent = HomeActivity.newIntent(this)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.putExtra(Constants.PushNotifications.APPOINTMENT, open)
        intent.putExtra(Constants.PushNotifications.RECORD, open2)

//        intent.addFlags(PendingIntent.FLAG_UPDATE_CURRENT)
//        val pendingIntent = PendingIntent.getActivity(
//                this, 0 /* Request code */, intent,
//                PendingIntent.FLAG_ONE_SHOT
//        )

        val pendingIntent =
            PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val channelId = getString(R.string.default_notification_channel_id)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setContentTitle(title)
            .setAutoCancel(true)
            .setContentIntent(pendingIntent)
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setCategory(NotificationCompat.CATEGORY_ALARM)
            .setChannelId(channelId)
            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText(messageBody)
            )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.drawable.ic_notification)
            notificationBuilder.color = ContextCompat.getColor(this, R.color.colorAccent)
        } else {
            notificationBuilder.setSmallIcon(R.drawable.ic_notification)
        }

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                channelId,
                NotificationManager.IMPORTANCE_HIGH
            )
            channel.enableLights(true)
            channel.lightColor = Color.CYAN
            channel.enableVibration(true)
            channel.canBypassDnd()
            channel.importance = NotificationManager.IMPORTANCE_HIGH
            channel.setShowBadge(false)
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
    }

    private fun sendBroadCast(context: Context, msg: String) {
        val broadcastIntent = Intent()
        broadcastIntent.action = Constants.PushNotifications.NOTIF_UPDATED
        broadcastIntent.putExtra(Constants.PushNotifications.BODY, msg)
        context.sendBroadcast(broadcastIntent)
    }

}