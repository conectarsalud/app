package com.tdp2.conectarsalud.presentation.ui.main.appointment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.tdp2.conectarsalud.R
import com.tdp2.conectarsalud.data.local.model.Appointment
import com.tdp2.conectarsalud.data.local.model.AppointmentState
import com.tdp2.conectarsalud.presentation.ui.base.BaseActivity
import com.tdp2.conectarsalud.presentation.ui.videocall.VideoCallActivity
import com.tdp2.conectarsalud.utils.Constants
import com.tdp2.utils.extensions.*
import kotlinx.android.synthetic.main.activity_queue.*
import javax.inject.Inject

class QueueActivity : BaseActivity() {

    companion object {
        fun newIntent(context: Context, appointment: Appointment): Intent {
            val intent = Intent(context, QueueActivity::class.java)
            intent.putExtra(Constants.APPOINTMENT, appointment)
            return intent
        }

        private const val TAG = "QUEUE_ACT"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var queueViewModel: QueueViewModel
    private lateinit var mAppointment: Appointment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_queue)

        queueViewModel =
            ViewModelProvider(this, viewModelFactory).get(QueueViewModel::class.java)

        intent.getParcelableExtra<Appointment>(Constants.APPOINTMENT)?.apply {
            mAppointment = this
        }

        setupToolbar(toolbar, R.string.app_name)
        setSupportActionBar(toolbar)

        setupListeners()
        setupObservers()

        queueViewModel.getQueue(mAppointment.id)
    }

    private fun setupListeners() {

        swipeRefreshLayout.setOnRefreshListener {
            queueViewModel.getQueue(mAppointment.id)
        }

        initVideoCallButton.setOnClickListener {
            val intent = VideoCallActivity.newIntent(this, mAppointment)
            startActivity(intent)
            finish()
        }
    }

    private fun setupObservers() {

        queueViewModel.dataLoading.observe(this, Observer { loading ->
            when (loading) {
                true -> swipeRefreshLayout.isRefreshing = true
                false -> swipeRefreshLayout.isRefreshing = false
            }
        })

        queueViewModel.appointment.observe(this, Observer { appointment ->
            when (appointment.state) {
                AppointmentState.Queue -> onQueue(appointment)
                AppointmentState.Ready -> onReady(appointment)
                AppointmentState.Ongoing -> onReady(appointment)
                else -> {
                }
            }
        })

        mainView.setupSnackbar(this, queueViewModel.snackBarMessage, Snackbar.LENGTH_LONG)
        mainView.setupToast(this, queueViewModel.toastMessage, Toast.LENGTH_LONG)
        mainView.setupErrorDialog(this, queueViewModel.errorDialog)
        mainView.setupWarningDialog(this, queueViewModel.warningDialog)
    }

    private fun onQueue(appointment: Appointment) {
        queueTextView.text =
            "${resources.getString(R.string.queue_1)} ${appointment.queue} ${resources.getString(R.string.queue_2)}"
    }

    private fun onReady(appointment: Appointment) {

        initVideoCallButton.visibility = View.VISIBLE
        queueTextView.text = resources.getString(R.string.queue_ready)
        queueDescription.visibility = View.GONE
        val stb = AnimationUtils.loadAnimation(this, R.anim.button)
        initVideoCallButton.startAnimation(stb)
        queueViewModel.setAppointment(appointment)
        mAppointment = appointment
    }
}