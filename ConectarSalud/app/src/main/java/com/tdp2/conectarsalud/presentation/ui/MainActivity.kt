package com.tdp2.conectarsalud.presentation.ui

import android.os.Bundle
import com.tdp2.conectarsalud.R
import com.tdp2.conectarsalud.presentation.ui.base.BaseActivity

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
