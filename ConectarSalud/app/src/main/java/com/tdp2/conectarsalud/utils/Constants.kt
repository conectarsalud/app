package com.tdp2.conectarsalud.utils

class Constants {

    class StatusCode {
        companion object {
            const val UNAUTHORIZED = 401
            const val INVALID_APPOINTMENT_ACTION = 500
        }
    }

    class Format {
        companion object {
            const val DDMMMYYYY = "dd MMM yyyy"
            const val YYYYMMDD = "yyyy-MM-dd"
            const val MMMDD = "MMM dd"
            const val TIMEFORMAT = "hh:mm a"
            const val DATETIMEUTC = "yyyy-MM-dd'T'HH:mm:ssZ"
        }
    }

    companion object {
        const val APPOINTMENT = "APPOINTMENT"
        const val RECORD = "RECORD"
    }

    class PushNotifications {
        companion object {
            const val TYPE = "type"
            const val NOTIF_UPDATED = "notification"
            const val BODY = "body"
            const val APPOINTMENT = "appointment_id"
            const val RECORD = "appointment"
        }
    }
}