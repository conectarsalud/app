package com.tdp2.conectarsalud.data.local.model

enum class AppointmentState(val state: String) {
    Queue("En cola"),
    Ready("Esperando afiliado"),
    Ongoing("En curso"),
    Finish("Terminado"),
    Canceled("Cancelado")
}