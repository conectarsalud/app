package com.tdp2.conectarsalud.data.remote.model.request

class LoginRequest (val username: String, val password: String)