package com.tdp2.conectarsalud.data.remote.model.request

class RatingRequest(val rating: Int, val comments: String?)