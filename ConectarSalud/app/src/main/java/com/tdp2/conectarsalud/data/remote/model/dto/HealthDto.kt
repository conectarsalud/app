package com.tdp2.conectarsalud.data.remote.model.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NewAppointmentDto(
    @Json(name = "appointment") val appointment: AppointmentDto
)

@JsonClass(generateAdapter = true)
data class AppointmentDto(
    @Json(name = "id") val id: Int,
    @Json(name = "doctor_id") val doctorId: Int?,
    @Json(name = "estado") val state: String,
    @Json(name = "channel_id") val channelId: String?,
    @Json(name = "personas_en_espera") val queue: String?
)

@JsonClass(generateAdapter = true)
data class SpecialtiesDto(
    @Json(name = "medical_specialties") val specialties: List<SpecialtyDto>
)

@JsonClass(generateAdapter = true)
data class SpecialtyDto(
    @Json(name = "medical_specialty_id") val id: Int,
    @Json(name = "name") val name: String
)

@JsonClass(generateAdapter = true)
data class RatingAppointmentDto(
    @Json(name = "appointment_rating") val appointment: RatedAppointmentDto
)

@JsonClass(generateAdapter = true)
data class RatedAppointmentDto(
    @Json(name = "appointment_id") val id: Int,
    @Json(name = "rating") val rating: Int,
    @Json(name = "comments") val comments: String?
)

@JsonClass(generateAdapter = true)
data class RecordsDto(
    @Json(name = "appointments") val records: List<RecordDto>
)

@JsonClass(generateAdapter = true)
data class RecordDto(
    @Json(name = "id") val id: Int,
    @Json(name = "especialidad") val specialty: String,
    @Json(name = "doctor") val doctor: DoctorDto,
    @Json(name = "motivo") val reason: String,
    @Json(name = "hora_solicitud") val date: String,
    @Json(name = "informacion") val information: InformationDto
)

@JsonClass(generateAdapter = true)
data class InformationDto(
    @Json(name = "indicaciones") val indications: String?,
    @Json(name = "recetas") val prescriptions: List<String>?
)

@JsonClass(generateAdapter = true)
data class DoctorDto(
    @Json(name = "doctor_id") val id: Int,
    @Json(name = "nombre") val name: String
)

@JsonClass(generateAdapter = true)
data class NotificationDto(
    @Json(name = "member_device_token") val data: NotificationTokenDto
)

@JsonClass(generateAdapter = true)
data class NotificationTokenDto(
    @Json(name = "member_id") val id: Int,
    @Json(name = "device_token") val token: String
)