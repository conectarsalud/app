package com.tdp2.conectarsalud.presentation.ui.main.records

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.tdp2.conectarsalud.R
import com.tdp2.conectarsalud.data.local.model.Record
import kotlinx.android.synthetic.main.item_record.view.*

class RecordAdapter(private val listener: (Record, View) -> Unit) :
    RecyclerView.Adapter<RecordViewHolder>() {

    private val diffCallback = object : DiffUtil.ItemCallback<Record>() {

        override fun areItemsTheSame(oldItem: Record, newItem: Record): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Record, newItem: Record): Boolean {
            return oldItem == newItem
        }

    }
    private val differ = AsyncListDiffer(this, diffCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecordViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.item_record, parent, false)
        return RecordViewHolder(view, listener)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    override fun onBindViewHolder(holder: RecordViewHolder, position: Int) {

        holder.bind(differ.currentList[position])
    }

    fun refresh(list: List<Record>) {
        differ.submitList(list)
    }
}

class RecordViewHolder(view: View, listener: (Record, View) -> Unit) :
    RecyclerView.ViewHolder(view) {

    var item: Record? = null

    init {
        itemView.setOnClickListener {
            item?.let {
                listener(it, itemView)
            }
        }
    }

    fun bind(record: Record) {

        item = record

        val date = record.date.split(" ").first()

        itemView.specialtyTextView.text = record.specialty
        itemView.doctorTextView.text = record.doctor.name
        itemView.dateTextView.text = date
    }
}