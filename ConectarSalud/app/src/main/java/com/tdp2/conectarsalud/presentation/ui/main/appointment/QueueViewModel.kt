package com.tdp2.conectarsalud.presentation.ui.main.appointment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.tdp2.conectarsalud.R
import com.tdp2.conectarsalud.data.local.model.Appointment
import com.tdp2.conectarsalud.data.mapper.toAppointment
import com.tdp2.conectarsalud.data.remote.utils.NetworkConnectionException
import com.tdp2.conectarsalud.data.remote.utils.Result
import com.tdp2.conectarsalud.data.repository.HealthRepository
import com.tdp2.conectarsalud.presentation.ui.base.BaseViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

class QueueViewModel
@Inject constructor(private val healthRepository: HealthRepository) : BaseViewModel() {

    private val TAG: String = "QUEUE_VM"

    private val mAppointment = MutableLiveData<Appointment>()
    val appointment: LiveData<Appointment> = mAppointment

    fun setAppointment(appointment: Appointment) {
        healthRepository.setAppointment(appointment)
    }

    fun getQueue(appointmentId: String) {

        mDataLoading.value = true
        viewModelScope.launch {
            when (val result = healthRepository.getQueue(appointmentId)) {
                // Successful HTTP result
                is Result.Ok -> {
                    mAppointment.value = result.value.appointment.toAppointment()
                }
                // Any HTTP error
                is Result.Error -> {
                    showErrorDialog(R.string.loading_error)
                }
                // Exception while request invocation
                is Result.Exception -> {
                    when (result.exception) {
                        is NetworkConnectionException -> showErrorDialog(R.string.connection_error)
                        else -> showErrorDialog(R.string.loading_error)
                    }
                }
            }
            mDataLoading.value = false
        }
    }
}