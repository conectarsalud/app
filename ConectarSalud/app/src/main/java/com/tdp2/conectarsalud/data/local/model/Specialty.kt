package com.tdp2.conectarsalud.data.local.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Specialty(
    val id: Int,
    val name: String
) : Parcelable