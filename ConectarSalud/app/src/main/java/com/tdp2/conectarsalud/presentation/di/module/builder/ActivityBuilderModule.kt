package com.tdp2.conectarsalud.presentation.di.module.builder

import com.tdp2.conectarsalud.presentation.ui.MainActivity
import com.tdp2.conectarsalud.presentation.ui.access.LoginActivity
import com.tdp2.conectarsalud.presentation.ui.main.appointment.AppointmentActivity
import com.tdp2.conectarsalud.presentation.ui.main.appointment.QueueActivity
import com.tdp2.conectarsalud.presentation.ui.main.home.HomeActivity
import com.tdp2.conectarsalud.presentation.ui.main.records.RecordsActivity
import com.tdp2.conectarsalud.presentation.ui.main.records.detail.RecordsDetailActivity
import com.tdp2.conectarsalud.presentation.ui.videocall.RatingActivity
import com.tdp2.conectarsalud.presentation.ui.videocall.VideoCallActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ActivityBuilderModule {

    //Generates an AndroidInjector for the return type of this method
    // Crea un Injector para el tipo que se retorna, lo que va a permitir luego inyectar
    // Las dependencias a la activity

    @ContributesAndroidInjector(modules = [FragmentBuilderModule::class])
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [FragmentBuilderModule::class])
    abstract fun contributeLoginActivity(): LoginActivity

    @ContributesAndroidInjector(modules = [FragmentBuilderModule::class])
    abstract fun contributeHomeActivity(): HomeActivity

    @ContributesAndroidInjector(modules = [FragmentBuilderModule::class])
    abstract fun contributeVideoCallActivity(): VideoCallActivity

    @ContributesAndroidInjector(modules = [FragmentBuilderModule::class])
    abstract fun contributeRatingActivity(): RatingActivity

    @ContributesAndroidInjector(modules = [FragmentBuilderModule::class])
    abstract fun contributeAppointmentActivity(): AppointmentActivity

    @ContributesAndroidInjector(modules = [FragmentBuilderModule::class])
    abstract fun contributeQueueActivity(): QueueActivity

    @ContributesAndroidInjector(modules = [FragmentBuilderModule::class])
    abstract fun contributeRecordsActivity(): RecordsActivity

    @ContributesAndroidInjector(modules = [FragmentBuilderModule::class])
    abstract fun contributeRecordsDetailActivity(): RecordsDetailActivity

}