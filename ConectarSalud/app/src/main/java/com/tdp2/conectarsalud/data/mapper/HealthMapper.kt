package com.tdp2.conectarsalud.data.mapper

import com.tdp2.conectarsalud.data.local.model.*
import com.tdp2.conectarsalud.data.remote.model.dto.*

fun HospitalDto.toHospital(): Hospital {

    return Hospital(
        id = this.id,
        name = this.name,
        address = this.address,
        phoneNumber = this.phoneNumber,
        guardPhoneNumber = this.guardPhoneNumber,
        specialty = this.specialty,
        neighborhood = this.neighborhood,
        district = this.district,
        lat = this.lat.toDouble(),
        long = this.long.toDouble()
    )
}

fun SpecialtyDto.toSpecialty(): Specialty {

    return Specialty(
        id = this.id,
        name = this.name
    )
}

fun AppointmentDto.toAppointment(): Appointment {

    return Appointment(
        id = this.id.toString(),
        doctorId = this.doctorId.toString(),
        state = this.state.toAppointmentState(),
        channelId = this.channelId,
        queue = this.queue
    )
}

fun RecordDto.toRecord(): Record {

    return Record(
        id = this.id,
        specialty = this.specialty,
        doctor = this.doctor.toDoctor(),
        reason = this.reason,
        date = this.date,
        indications = this.information.indications,
        prescriptions = this.information.prescriptions
    )
}

fun DoctorDto.toDoctor(): Doctor {

    return Doctor(
        id = this.id,
        name = this.name
    )
}

fun String.toAppointmentState(): AppointmentState {
    return when (this) {
        AppointmentState.Queue.state -> AppointmentState.Queue
        AppointmentState.Ready.state -> AppointmentState.Ready
        AppointmentState.Ongoing.state -> AppointmentState.Ongoing
        AppointmentState.Finish.state -> AppointmentState.Finish
        AppointmentState.Canceled.state -> AppointmentState.Canceled
        else -> AppointmentState.Finish
    }
}