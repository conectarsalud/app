package com.tdp2.conectarsalud.presentation.ui.main.records

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.tdp2.conectarsalud.R
import com.tdp2.conectarsalud.presentation.ui.base.BaseActivity
import com.tdp2.conectarsalud.presentation.ui.main.records.detail.RecordsDetailActivity
import com.tdp2.utils.extensions.*
import kotlinx.android.synthetic.main.activity_records.*
import javax.inject.Inject

class RecordsActivity : BaseActivity() {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, RecordsActivity::class.java)
        }

        private const val TAG = "RECORDS_ACT"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var recordsViewModel: RecordsViewModel
    private lateinit var recordAdapter: RecordAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_records)

        recordsViewModel =
            ViewModelProvider(this, viewModelFactory).get(RecordsViewModel::class.java)

        init()
        setupListeners()
        setupObservers()

        recordsViewModel.getRecords()
    }

    private fun init() {

        setupToolbar(toolbar, R.string.records)
        setSupportActionBar(toolbar)
        addBackButton(toolbar)

        recordAdapter = RecordAdapter { element, view ->
            val intent = RecordsDetailActivity.newIntent(this, element)
            startActivity(intent)
        }
        recordsRecyclerView.layoutManager = LinearLayoutManager(this)
        recordsRecyclerView.adapter = recordAdapter
    }

    private fun setupListeners() {

    }

    private fun setupObservers() {

        recordsViewModel.dataLoading.observe(this@RecordsActivity, Observer { loading ->
            when (loading) {
                true -> loadingContentView.visible(true)
                false -> loadingContentView.gone(true)
            }
        })

        recordsViewModel.records.observe(this@RecordsActivity, Observer { records ->

            recordAdapter.refresh(records)
        })

        mainView.setupSnackbar(this, recordsViewModel.snackBarMessage, Snackbar.LENGTH_LONG)
        mainView.setupToast(this, recordsViewModel.toastMessage, Toast.LENGTH_LONG)
        mainView.setupErrorDialog(this, recordsViewModel.errorDialog)
    }
}