package com.tdp2.conectarsalud.presentation.ui.videocall

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.tdp2.conectarsalud.R
import com.tdp2.conectarsalud.data.local.model.Appointment
import com.tdp2.conectarsalud.data.mapper.toAppointment
import com.tdp2.conectarsalud.data.remote.utils.NetworkConnectionException
import com.tdp2.conectarsalud.data.remote.utils.Result
import com.tdp2.conectarsalud.data.repository.HealthRepository
import com.tdp2.conectarsalud.presentation.ui.base.BaseViewModel
import com.tdp2.conectarsalud.utils.Constants
import kotlinx.coroutines.launch
import javax.inject.Inject

class VideoCallViewModel
@Inject constructor(private val healthRepository: HealthRepository) : BaseViewModel() {

    private val TAG: String = "VIDEO_CALL_VM"

    fun makeSnackBar(msg: String) {
        showSnackbarMessage(msg)
    }

    fun makeToast(msg: String) {
        showToastMessage(msg)
    }

    private val mStartAppointment = MutableLiveData<Appointment>()
    val startAppointment: LiveData<Appointment>
        get() = mStartAppointment

    private val mFinishAppointment = MutableLiveData<Appointment>()
    val finishAppointment: LiveData<Appointment>
        get() = mFinishAppointment

    fun clearAppointment() {
        healthRepository.clearAppointment()
    }

    fun startAppointment(appointmentId: String) {

        mDataLoading.value = true
        viewModelScope.launch {
            when (val result = healthRepository.startAppointment(appointmentId)) {
                // Successful HTTP result
                is Result.Ok -> {
                    mStartAppointment.value = result.value.appointment.toAppointment()
                }
                // Any HTTP error
                is Result.Error -> {
                    showErrorDialog(R.string.loading_error)
                }
                // Exception while request invocation
                is Result.Exception -> {
                    when (result.exception) {
                        is NetworkConnectionException -> showErrorDialog(R.string.connection_error)
                        else -> showErrorDialog(R.string.loading_error)
                    }
                }
            }
            mDataLoading.value = false
        }
    }

    fun finishAppointment(appointmentId: String) {

        mDataLoading.value = true
        viewModelScope.launch {
            when (val result = healthRepository.finishAppointment(appointmentId)) {
                // Successful HTTP result
                is Result.Ok -> {
                    mFinishAppointment.value = result.value.appointment.toAppointment()
                }
                // Any HTTP error
                is Result.Error -> {
                    when (result.exception.code()) {
                        Constants.StatusCode.INVALID_APPOINTMENT_ACTION -> showWarningDialog(R.string.invalid_appointment_action)
                        else -> showErrorDialog(R.string.loading_error)
                    }
                }
                // Exception while request invocation
                is Result.Exception -> {
                    when (result.exception) {
                        is NetworkConnectionException -> showErrorDialog(R.string.connection_error)
                        else -> showErrorDialog(R.string.loading_error)
                    }
                }
            }
            mDataLoading.value = false
        }
    }
}