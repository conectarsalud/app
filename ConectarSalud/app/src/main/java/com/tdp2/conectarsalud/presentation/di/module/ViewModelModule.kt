package com.tdp2.conectarsalud.presentation.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tdp2.conectarsalud.presentation.di.base.ViewModelKey
import com.tdp2.conectarsalud.presentation.ui.access.LoginViewModel
import com.tdp2.conectarsalud.presentation.ui.main.appointment.AppointmentViewModel
import com.tdp2.conectarsalud.presentation.ui.main.appointment.QueueViewModel
import com.tdp2.conectarsalud.presentation.ui.main.home.HomeViewModel
import com.tdp2.conectarsalud.presentation.ui.main.records.RecordsViewModel
import com.tdp2.conectarsalud.presentation.ui.main.records.detail.RecordsDetailViewModel
import com.tdp2.conectarsalud.presentation.ui.videocall.RatingViewModel
import com.tdp2.conectarsalud.presentation.ui.videocall.VideoCallViewModel
import com.tdp2.conectarsalud.utils.AppViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {

    //@Bind es simil a provider, devuelve la interfaz y recibe una implementación de la misma (esto obliga a que la clase sea abstracta).

    //Injecta este objeto en un Map usando ViewModelKey como key y el Provider como value.
    // El provider va a crear el objeto

    @Binds
    abstract fun bindViewModelFactory(factory: AppViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(loginViewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindHomeViewModel(homeViewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VideoCallViewModel::class)
    abstract fun bindVideoCallViewModel(videoCallViewModel: VideoCallViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RatingViewModel::class)
    abstract fun bindRatingViewModel(ratingViewModel: RatingViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AppointmentViewModel::class)
    abstract fun bindAppointmentViewModel(appointmentViewModel: AppointmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(QueueViewModel::class)
    abstract fun bindQueueViewModel(queueViewModel: QueueViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RecordsViewModel::class)
    abstract fun bindRecordsViewModel(recordsViewModel: RecordsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RecordsDetailViewModel::class)
    abstract fun bindRecordsDetailViewModel(recordsDetailViewModel: RecordsDetailViewModel): ViewModel


}