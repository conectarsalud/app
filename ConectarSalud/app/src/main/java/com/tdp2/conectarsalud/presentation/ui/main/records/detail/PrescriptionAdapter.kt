package com.tdp2.conectarsalud.presentation.ui.main.records.detail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.tdp2.conectarsalud.R
import kotlinx.android.synthetic.main.item_prescription.view.*

class PrescriptionAdapter(private val listener: (String, View) -> Unit) :
    RecyclerView.Adapter<PrescriptionViewHolder>() {

    private val diffCallback = object : DiffUtil.ItemCallback<String>() {

        override fun areItemsTheSame(oldItem: String, newItem: String): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: String, newItem: String): Boolean {
            return oldItem == newItem
        }
    }
    private val differ = AsyncListDiffer(this, diffCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PrescriptionViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.item_prescription, parent, false)
        return PrescriptionViewHolder(view, listener)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    override fun onBindViewHolder(holder: PrescriptionViewHolder, position: Int) {

        holder.bind(differ.currentList[position])
    }

    fun refresh(list: List<String>) {
        differ.submitList(list)
    }
}

class PrescriptionViewHolder(view: View, listener: (String, View) -> Unit) :
    RecyclerView.ViewHolder(view) {

    var item: String? = null

    init {
        itemView.setOnClickListener {
            item?.let {
                listener(it, itemView)
            }
        }
    }

    fun bind(prescription: String) {

        item = prescription

        itemView.prescriptionImageView.setImageURI(prescription)
    }
}