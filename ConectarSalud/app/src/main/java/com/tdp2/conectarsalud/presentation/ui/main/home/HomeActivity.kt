package com.tdp2.conectarsalud.presentation.ui.main.home

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.*
import android.graphics.Color
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.GsonBuilder
import com.tdp2.conectarsalud.R
import com.tdp2.conectarsalud.data.local.model.RecordNotification
import com.tdp2.conectarsalud.data.local.model.toRecord
import com.tdp2.conectarsalud.presentation.ui.access.LoginActivity
import com.tdp2.conectarsalud.presentation.ui.base.BaseActivity
import com.tdp2.conectarsalud.presentation.ui.main.appointment.AppointmentActivity
import com.tdp2.conectarsalud.presentation.ui.main.appointment.QueueActivity
import com.tdp2.conectarsalud.presentation.ui.main.records.RecordsActivity
import com.tdp2.conectarsalud.presentation.ui.main.records.detail.RecordsDetailActivity
import com.tdp2.conectarsalud.utils.Constants
import com.tdp2.utils.extensions.*
import com.tdp2.utils.ui.AlertDialog
import kotlinx.android.synthetic.main.activity_home.*
import javax.inject.Inject

class HomeActivity : BaseActivity() {

    companion object {
        fun newIntent(context: Context, params: Bundle? = null): Intent {
            val intent = Intent(context, HomeActivity::class.java)
            params?.apply { intent.putExtras(this) }
            return intent
        }

        private const val TAG = "HOME_ACT"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var homeViewModel: HomeViewModel

    private var filter: IntentFilter =
        IntentFilter().apply { addAction(Constants.PushNotifications.NOTIF_UPDATED) }
    private val myReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            intent.extras?.get(Constants.PushNotifications.BODY)?.apply {
                /*Toast.makeText(
                    this@HomeActivity, this.toString(),
                    Toast.LENGTH_SHORT
                ).show()*/
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        homeViewModel = ViewModelProvider(this, viewModelFactory).get(HomeViewModel::class.java)

        setupToolbar(toolbar, R.string.app_name)
        setSupportActionBar(toolbar)

        setupListeners()
        setupObservers()
        checkNotification()
    }

    private fun checkNotification() {
        intent.getStringExtra(Constants.PushNotifications.APPOINTMENT)?.let {
            if (homeViewModel.hasAppointment()) {
                val appointment = homeViewModel.getAppointment()
                if (appointment.id == it) {
                    val intent = QueueActivity.newIntent(this, appointment)
                    startActivity(intent)
                }
            }
        }
        intent.getStringExtra(Constants.PushNotifications.RECORD)?.let {
            if (!homeViewModel.hasAppointment()) {
                gotoRecordsDetail(it)
            }
        }
    }

    private fun gotoRecordsDetail(data: String?) {
        if (!data.isNullOrEmpty()) {
            val recordNotification =
                GsonBuilder().create().fromJson(data, RecordNotification::class.java)
            val intent = RecordsDetailActivity.newIntent(this, recordNotification.toRecord())
            startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(myReceiver, filter)
        setupPushNotifications()
    }

    override fun onPause() {
        unregisterReceiver(myReceiver)
        super.onPause()
    }

    private fun setupListeners() {

        newAppointmentButton.setOnClickListener {
            if (homeViewModel.hasAppointment()) {
                val intent = QueueActivity.newIntent(this, homeViewModel.getAppointment())
                startActivity(intent)
            } else {
                val intent = AppointmentActivity.newIntent(this)
                startActivity(intent)
            }
        }

        previousAppointmentsButton.setOnClickListener {
            val intent = RecordsActivity.newIntent(this)
            startActivity(intent)
        }
    }

    private fun setupObservers() {

        homeViewModel.dataLoading.observe(this@HomeActivity, Observer { loading ->
            when (loading) {
                true -> loadingContentView.visible(true)
                false -> loadingContentView.gone(true)
            }
        })

        homeViewModel.cancelAppointment.observe(this, Observer { appointment ->
            homeViewModel.clearAppointment()
            logout()
        })

        mainView.setupSnackbar(this, homeViewModel.snackBarMessage, Snackbar.LENGTH_LONG)
        mainView.setupToast(this, homeViewModel.toastMessage, Toast.LENGTH_LONG)
        mainView.setupWarningDialog(this, homeViewModel.warningDialog)
        mainView.setupErrorDialog(this, homeViewModel.errorDialog)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menu?.apply { menuInflater.inflate(R.menu.home_menu, this) }

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.logout -> optionLogout()
        }

        return super.onOptionsItemSelected(item)
    }

    private fun makeLogoutDialog() {
        AlertDialog.Builder(this)
            .setTitle(R.string.alert)
            .setMessage(R.string.home_logout_warning)
            .setShowBackButton(false)
            .setPositiveButton(R.string.yes, DialogInterface.OnClickListener { dialog, which ->
                val appointment = homeViewModel.getAppointment()
                homeViewModel.cancelAppointment(appointment.id)
                dialog.dismiss()
            })
            .setNegativeButton(R.string.no, DialogInterface.OnClickListener { dialog, which ->
                dialog.dismiss()
            })
            .create().show()
    }

    private fun optionLogout() {
        when (homeViewModel.hasAppointment()) {
            true -> makeLogoutDialog()
            false -> logout()
        }
    }

    private fun logout() {
        homeViewModel.logout()
        val intent = LoginActivity.newIntent(this)
        startActivity(intent)
        finish()
    }

    private fun setupPushNotifications() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            setupNotificationChannels()
        }

        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("PUSH_NOTIF", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                task.result?.token?.apply {
                    homeViewModel.registerToPushNotifications(this)
                    Log.e("APPXX token", this)
                }
                subscribeTopic()
            })
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun setupNotificationChannels() {
        val channelId = getString(R.string.default_notification_channel_id)
        val adminChannelName = getString(R.string.default_notification_channel_id)

        val attributes = AudioAttributes.Builder()
            .setUsage(AudioAttributes.USAGE_NOTIFICATION)
            .build()

        val adminChannel: NotificationChannel
        adminChannel =
            NotificationChannel(channelId, adminChannelName, NotificationManager.IMPORTANCE_HIGH)
        adminChannel.enableLights(true)
        adminChannel.lightColor = Color.CYAN
        adminChannel.enableVibration(true)
        adminChannel.importance = NotificationManager.IMPORTANCE_HIGH
        adminChannel.setSound(
            RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION),
            attributes
        )
        adminChannel.setShowBadge(false)

        val notificationManager = getSystemService(NotificationManager::class.java)
        notificationManager?.createNotificationChannel(adminChannel)
    }

    private fun subscribeTopic() {
        FirebaseMessaging.getInstance()
            .subscribeToTopic(getString(R.string.default_notification_channel_id))
            .addOnCompleteListener { task ->
                var msg = "Subscribing OK"
                if (!task.isSuccessful) {
                    msg = "Subscribing FAILED"
                }
                Log.d("PUSH_NOTIF", msg)
            }
    }
}
