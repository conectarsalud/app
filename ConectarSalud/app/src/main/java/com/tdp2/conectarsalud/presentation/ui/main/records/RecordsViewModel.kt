package com.tdp2.conectarsalud.presentation.ui.main.records

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.tdp2.conectarsalud.R
import com.tdp2.conectarsalud.data.local.model.Record
import com.tdp2.conectarsalud.data.mapper.toRecord
import com.tdp2.conectarsalud.data.remote.utils.NetworkConnectionException
import com.tdp2.conectarsalud.data.remote.utils.Result
import com.tdp2.conectarsalud.data.repository.HealthRepository
import com.tdp2.conectarsalud.presentation.ui.base.BaseViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

class RecordsViewModel
@Inject constructor(private val healthRepository: HealthRepository) : BaseViewModel() {

    private val TAG: String = "RECORDS_VM"

    private val mRecords = MutableLiveData<List<Record>>()
    val records: LiveData<List<Record>> = mRecords

    fun getRecords() {

        mDataLoading.value = true

        viewModelScope.launch {
            when (val result = healthRepository.getRecords()) {
                // Successful HTTP result
                is Result.Ok -> {
                    mRecords.value = result.value.records.map { it.toRecord() }
                }
                // Any HTTP error
                is Result.Error -> {
                    showErrorDialog(R.string.loading_error)
                }
                // Exception while request invocation
                is Result.Exception -> {
                    when (result.exception) {
                        is NetworkConnectionException -> showErrorDialog(R.string.connection_error)
                        else -> showErrorDialog(R.string.loading_error)
                    }
                }
            }
            mDataLoading.value = false
        }
    }
}