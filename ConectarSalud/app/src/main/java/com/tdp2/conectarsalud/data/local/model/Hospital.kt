package com.tdp2.conectarsalud.data.local.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Hospital(
    val id: String,
    val name: String,
    val address: String,
    val phoneNumber: String,
    val guardPhoneNumber: String,
    val specialty: String,
    val neighborhood: String,
    val district: String,
    val lat: Double,
    val long: Double
) : Parcelable