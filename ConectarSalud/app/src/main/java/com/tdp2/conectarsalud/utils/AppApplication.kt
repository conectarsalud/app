package com.tdp2.conectarsalud.utils

import android.content.Context
import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.stetho.Stetho
import com.google.firebase.FirebaseApp
import com.google.firebase.analytics.FirebaseAnalytics
import com.tdp2.conectarsalud.BuildConfig
import com.tdp2.conectarsalud.data.local.AppPreferences
import com.tdp2.conectarsalud.presentation.di.component.DaggerAppComponent
import dagger.android.support.DaggerApplication

class AppApplication : DaggerApplication() {

    val isAppInBackground: Boolean
        get() = lifecycleListener.isAppInBackground

    private val lifecycleListener: AppLifecycleListener by lazy {
        AppLifecycleListener()
    }

    init {
        instance = this
    }

    companion object {
        var instance: AppApplication? = null

        fun applicationContext(): Context {
            return instance!!.applicationContext
        }
    }

    private val applicationInjector = DaggerAppComponent.builder()
        .application(this)
        .build()

    override fun applicationInjector() = applicationInjector

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
            Log.e("DEBUG", "http://localhost:8080")
            Log.e("DEBUG", "adb forward tcp:8080 tcp:8080")
            Log.e("DEBUG", "chrome://inspect")
        }

        FirebaseAnalytics.getInstance(this)
        FirebaseApp.initializeApp(this)
        Fresco.initialize(this)
        AppPreferences.init(this)
        setupLifecycleListener()
    }

    private fun setupLifecycleListener() {
        ProcessLifecycleOwner.get().lifecycle
            .addObserver(lifecycleListener)
    }

    class AppLifecycleListener : LifecycleObserver {

        var isAppInBackground: Boolean = false

        @OnLifecycleEvent(Lifecycle.Event.ON_START)
        fun onMoveToForeground() {
            isAppInBackground = false
            Log.d("SampleLifecycle", "Returning to foreground…")
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
        fun onMoveToBackground() {
            isAppInBackground = true
            Log.d("SampleLifecycle", "Moving to background…")
        }
    }

}