package com.tdp2.conectarsalud.presentation.ui.main.appointment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.tdp2.conectarsalud.R
import com.tdp2.conectarsalud.data.local.model.Specialty
import com.tdp2.conectarsalud.presentation.ui.base.BaseActivity
import com.tdp2.utils.extensions.*
import kotlinx.android.synthetic.main.activity_appointment.*
import javax.inject.Inject

class AppointmentActivity : BaseActivity() {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, AppointmentActivity::class.java)
        }

        private const val TAG = "APPOINTMENT_ACT"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var appointmentViewModel: AppointmentViewModel
    private lateinit var specialtyAdapter: ArrayAdapter<String>
    private val specialties = mutableListOf<Specialty>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_appointment)

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE or WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        appointmentViewModel =
            ViewModelProvider(this, viewModelFactory).get(AppointmentViewModel::class.java)

        setupToolbar(toolbar, R.string.appointment)
        setSupportActionBar(toolbar)
        addBackButton(toolbar)

        init()
        setupListeners()
        setupObservers()
        appointmentViewModel.getSpecialties()
    }

    private fun init() {

        specialtyAdapter =
            ArrayAdapter(
                this,
                R.layout.support_simple_spinner_dropdown_item,
                arrayListOf(resources.getString(R.string.appointment_spinner))
            )
        specialtySpinner.adapter = specialtyAdapter
    }

    private fun setupListeners() {

        specialtySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                sendButton.isEnabled = false
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                sendButton.isEnabled = position != 0 && !reasonsEditText.text.isNullOrEmpty()
            }

        }

        reasonsEditText.afterTextChanged {
            sendButton.isEnabled =
                specialtySpinner.selectedItemPosition != 0 && !reasonsEditText.text.isNullOrEmpty()
        }

        sendButton.setOnClickListener {
            val selectedSpecialty =
                specialties.filter { it.name == specialtySpinner.selectedItem as String }
            appointmentViewModel.newAppointment(
                selectedSpecialty.first().id,
                reasonsEditText.text.toString()
            )
        }
    }

    private fun setupObservers() {

        appointmentViewModel.dataLoading.observe(this, Observer { loading ->
            when (loading) {
                true -> loadingContentView.visible(true)
                false -> loadingContentView.gone(true)
            }
        })

        appointmentViewModel.specialties.observe(this, Observer { list ->
            specialties.addAll(list)
            val names = list.map { it.name }
            specialtyAdapter.addAll(names)
            specialtyAdapter.notifyDataSetChanged()
        })

        appointmentViewModel.appointment.observe(this, Observer { appointment ->
            appointmentViewModel.setAppointment(appointment)
            val intent = QueueActivity.newIntent(this, appointment)
            startActivity(intent)
            finish()
        })

        mainView.setupSnackbar(this, appointmentViewModel.snackBarMessage, Snackbar.LENGTH_LONG)
        mainView.setupToast(this, appointmentViewModel.toastMessage, Toast.LENGTH_LONG)
        mainView.setupErrorDialog(this, appointmentViewModel.errorDialog)
        mainView.setupWarningDialog(this, appointmentViewModel.warningDialog)
    }
}