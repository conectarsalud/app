package com.tdp2.conectarsalud.presentation.ui.videocall

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.tdp2.conectarsalud.R
import com.tdp2.conectarsalud.data.local.model.Appointment
import com.tdp2.conectarsalud.presentation.ui.base.BaseActivity
import com.tdp2.conectarsalud.utils.Constants
import com.tdp2.utils.extensions.*
import kotlinx.android.synthetic.main.activity_rating.*
import javax.inject.Inject

class RatingActivity : BaseActivity() {

    companion object {
        fun newIntent(context: Context, appointment: Appointment): Intent {
            val intent = Intent(context, RatingActivity::class.java)
            intent.putExtra(Constants.APPOINTMENT, appointment)
            return intent
        }

        private const val TAG = "RATING_ACT"
        private const val MIN_RATING = 2
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var ratingViewModel: RatingViewModel
    private lateinit var mAppointment: Appointment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rating)

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE or WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        ratingViewModel = ViewModelProvider(this, viewModelFactory).get(RatingViewModel::class.java)

        setupToolbar(toolbar, R.string.app_name)
        setSupportActionBar(toolbar)

        intent.getParcelableExtra<Appointment>(Constants.APPOINTMENT)?.apply {
            mAppointment = this
        }

        setupListeners()
        setupObservers()
    }

    private fun setupListeners() {

        ratingBar.setOnRatingBarChangeListener { ratingBar, rating, fromUser ->
            sendButton.isEnabled = true
        }

        skipButton.setOnClickListener {
            finish()
        }

        sendButton.setOnClickListener {

            val rating = ratingBar.rating.toInt()
            if (rating <= MIN_RATING && commentsEditText.text.isNullOrEmpty()) {
                ratingViewModel.makeWarningDialog()
            } else {
                if (commentsEditText.text.isNullOrEmpty()) {
                    ratingViewModel.rateAppointment(mAppointment.id, rating)
                } else {
                    ratingViewModel.rateAppointment(
                        mAppointment.id,
                        rating,
                        commentsEditText.text.toString()
                    )
                }
            }
        }
    }

    private fun setupObservers() {

        ratingViewModel.dataLoading.observe(this, Observer { loading ->
            when (loading) {
                true -> loadingContentView.visible(true)
                false -> loadingContentView.gone(true)
            }
        })

        ratingViewModel.rating.observe(this, Observer { appointment ->
            Toast.makeText(this, R.string.rating_success, Toast.LENGTH_LONG).show()
            finish()
        })

        mainView.setupSnackbar(this, ratingViewModel.snackBarMessage, Snackbar.LENGTH_LONG)
        mainView.setupToast(this, ratingViewModel.toastMessage, Toast.LENGTH_LONG)
        mainView.setupErrorDialog(this, ratingViewModel.errorDialog)
        mainView.setupWarningDialog(this, ratingViewModel.warningDialog)
    }
}
