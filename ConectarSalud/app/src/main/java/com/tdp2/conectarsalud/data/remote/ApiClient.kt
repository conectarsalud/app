package com.tdp2.conectarsalud.data.remote

import com.tdp2.conectarsalud.data.remote.model.dto.*
import com.tdp2.conectarsalud.data.remote.model.request.AppointmentRequest
import com.tdp2.conectarsalud.data.remote.model.request.LoginRequest
import com.tdp2.conectarsalud.data.remote.model.request.RatingRequest
import com.tdp2.conectarsalud.data.remote.model.request.TokenRequest
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ApiClient {

    @GET("hospitals/all")
    fun getHospitalsData(): Call<HospitalsDataDto>

    @POST("member/login")
    fun login(@Body body: LoginRequest): Call<LoginDto>

    @GET("member/medical_specialties")
    fun getSpecialties(): Call<SpecialtiesDto>

    @POST("member/appointment")
    fun newAppointment(@Body body: AppointmentRequest): Call<NewAppointmentDto>

    @POST("member/appointment/{appointmentId}/start")
    fun startAppointment(@Path("appointmentId") id: String): Call<NewAppointmentDto>

    @POST("member/appointment/{appointmentId}/finish")
    fun finishAppointment(@Path("appointmentId") id: String): Call<NewAppointmentDto>

    @POST("member/appointment/{appointmentId}/cancel")
    fun cancelAppointment(@Path("appointmentId") id: String): Call<NewAppointmentDto>

    @POST("member/appointment/{appointmentId}/rating")
    fun rateAppointment(
        @Path("appointmentId") id: String,
        @Body body: RatingRequest
    ): Call<RatingAppointmentDto>

    @GET("member/appointments")
    fun getRecords(): Call<RecordsDto>

    @GET("member/appointment/{appointmentId}")
    fun getQueue(@Path("appointmentId") id: String): Call<NewAppointmentDto>

    @POST("member/register_device")
    fun registerDevice(@Body body: TokenRequest): Call<NotificationDto>
}