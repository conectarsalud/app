# Conectar Salud


## 1. Project Guidelines

### 1.1 Project Structure

The project should maintain the following structure:

	src/data
	src/presentation
	src/utils


#### 1.1.1 data
The data package contains any classes (and child packages) that are directly related to any kind of data or data management used within the app — be it networking classes and interfaces, preferences management, database classes, data models, network request and response model, or anything else directly tied to app data. Within this package we also have child packages that are organized per-type.

	src/data/local
	src/data/mapper
	src/data/remote
	src/data/repository
	
	
**local** - The local package contains all of the classes that deal with data being persisted locally to the application. 

	src/data/local/model
	src/data/local/sharedPreference

**mapper** - The Mapper package contains the implementations of the convertions of dto's objects to local model objects.
	
**remote** - The Remote package contains the implementation for the remote data source.
	
	src/data/remote/model
	src/data/remote/api
	
**repository** - The Repository package contains the classes or components that encapsulate the logic required to access data sources.
	
	
#### 1.1.2 presentation
The presentation package is responsible for holding any classes that are related to the UI components of the application. Within this package we also have child packages that are organized per-feature.

	src/presentation/di
	src/presentation/ui
	src/presentation/utils

**di** - The injection package holds all of our dependency injection classes.

	src/presentation/di/base
	src/presentation/di/module
	src/presentation/di/component
		
**ui** - Contains all of UI related components — such as Views, Activities, Fragments, ViewModels, etc. 

	src/presentation/ui/base
	src/presentation/ui/access
	
**base** - The base package is used to hold any base classes that can be extended by other classes within the UI package. For example, we have BaseActivity and BaseFragment classes which are used to hold common logic for the activities and fragment used by our app, meaning we don’t have to repeat the same code over and over for each new activity or fragment that we create.

**access** - The access package is used to hold any classes that make up the login UI, like for example login and registration.

---